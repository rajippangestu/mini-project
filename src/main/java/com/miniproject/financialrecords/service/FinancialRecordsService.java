package com.miniproject.financialrecords.service;

import com.miniproject.financialrecords.dao.CategoryInDao;
import com.miniproject.financialrecords.dao.CategoryOutDao;
import com.miniproject.financialrecords.dao.FinancialRecordsDao;
import com.miniproject.financialrecords.dao.FinancialTypeDao;
import com.miniproject.financialrecords.dto.FinancialRecordsRequest;
import com.miniproject.financialrecords.dto.FinancialRecordsResponse;
import com.miniproject.financialrecords.dto.TypeRecordResponse;
import com.miniproject.financialrecords.exception.BusinessException;
import com.miniproject.financialrecords.model.CategoryIn;
import com.miniproject.financialrecords.model.CategoryOut;
import com.miniproject.financialrecords.model.DailyRecords;
import com.miniproject.financialrecords.model.FinancialType;
import com.miniproject.financialrecords.utils.JsonUtils;
import com.miniproject.financialrecords.utils.MapConverterUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;


@Slf4j
@Service
@AllArgsConstructor
public class FinancialRecordsService {
    private final FinancialRecordsDao financialRecordsDao;
    private final FinancialTypeDao financialTypeDao;
    private final CategoryInDao categoryInDao;
    private final CategoryOutDao categoryOutDao;


    public Page<FinancialRecordsResponse> findAllPage ( LocalDate dateFrom, LocalDate dateTo, Pageable pageable){
        log.info("FinancialRecordsService::findAll");
        return financialRecordsDao.findAll(dateFrom, dateTo, pageable)
                .map(dailyRecords ->{
                    FinancialRecordsResponse response = new FinancialRecordsResponse();
                    response.setId(MapConverterUtils.setStringMap(dailyRecords.get("id")));
                    response.setDate(MapConverterUtils.setLocalDate(dailyRecords.get("date")));
                    response.setType(MapConverterUtils.setStringMap(dailyRecords.get("type_id")));
                    response.setCategoryIn(MapConverterUtils.setStringMap(dailyRecords.get("cat_in")));
                    response.setCategoryOut(MapConverterUtils.setStringMap(dailyRecords.get("cat_out")));
                    response.setAmount(MapConverterUtils.setBigDecimalMap(dailyRecords.get("amount")));
                    response.setInfo(MapConverterUtils.setStringMap(dailyRecords.get("information")));
                    return response;
                });
    }

    public Page<TypeRecordResponse> findAllPageIn ( LocalDate dateFrom, LocalDate dateTo, Pageable pageable){
        log.info("FinancialRecordsService::findAllIn");
        return financialRecordsDao.findAllIn(dateFrom, dateTo, pageable)
                .map(dailyRecords ->{
                    TypeRecordResponse response = new TypeRecordResponse();
                    response.setId(MapConverterUtils.setStringMap(dailyRecords.get("id")));
                    response.setDate(MapConverterUtils.setLocalDate(dailyRecords.get("date")));
                    response.setCategory(MapConverterUtils.setStringMap(dailyRecords.get("cat_in")));
                    response.setAmount(MapConverterUtils.setBigDecimalMap(dailyRecords.get("amount")));
                    response.setInfo(MapConverterUtils.setStringMap(dailyRecords.get("information")));
                    return response;
                });
    }

    public Page<TypeRecordResponse> findAllPageOut (LocalDate dateFrom, LocalDate dateTo, Pageable pageable){
        log.info("FinancialRecordsService::findAllOut");
        return financialRecordsDao.findAllOut(dateFrom, dateTo, pageable)
                .map(dailyRecords ->{
                    TypeRecordResponse response = new TypeRecordResponse();
                    response.setId(MapConverterUtils.setStringMap(dailyRecords.get("id")));
                    response.setDate(MapConverterUtils.setLocalDate(dailyRecords.get("date")));
                    response.setCategory(MapConverterUtils.setStringMap(dailyRecords.get("cat_out")));
                    response.setAmount(MapConverterUtils.setBigDecimalMap(dailyRecords.get("amount")));
                    response.setInfo(MapConverterUtils.setStringMap(dailyRecords.get("information")));
                    return response;
                });
    }

    public Optional<Map<String, Object>> findRecordsById(String id) {
        return financialRecordsDao.findRecordDailyById(id);
    }

    //In
    @Transactional
    public String createIn(FinancialRecordsRequest request){
        log.info("FinancialRecordsService::create catIn \nrequest json ={}", JsonUtils.objectToStringJson(request));

        DailyRecords model = new DailyRecords();
        FinancialType type = financialTypeDao.findById(request.getType())
                .orElseThrow(()-> new BusinessException("Data type tidak ada"));
        CategoryIn category = categoryInDao.findById(request.getCategory())
                .orElseThrow(()-> new BusinessException("Data category tidak ada"));

        model.setDate(request.getDate());
        model.setType(type);
        model.setCategoryIn(category);
        model.setAmount(request.getAmount());
        model.setInfo(request.getInfo());
        DailyRecords saved = financialRecordsDao.save(model);
        return saved.getId();
    }

    public String updateIn (String id, FinancialRecordsRequest request){
        log.info("FinancialRecordsService::update \nrequest json ={}", JsonUtils.objectToStringJson(request));
        DailyRecords model = financialRecordsDao.findById(id)
                .orElseThrow(()-> new BusinessException("Data id daily record tidak ada"));
        FinancialType type = financialTypeDao.findById(request.getType())
                .orElseThrow(()-> new BusinessException("Data type tidak ada"));
        CategoryIn category = categoryInDao.findById(request.getCategory())
                .orElseThrow(()-> new BusinessException("Data category tidak ada"));

        model.setDate(request.getDate());
        model.setType(type);
        model.setCategoryIn(category);
        model.setAmount(request.getAmount());
        model.setInfo(request.getInfo());
        DailyRecords saved = financialRecordsDao.save(model);
        return saved.getId();

    }

    //Out
    @Transactional
    public String createOut (FinancialRecordsRequest request){
        log.info("FinancialRecordsService::create catOut \nrequest json ={}", JsonUtils.objectToStringJson(request));

        FinancialType type = financialTypeDao.findById(request.getType())
                .orElseThrow(()-> new BusinessException("Data type tidak ada"));
        CategoryOut category = categoryOutDao.findById(request.getCategory())
                .orElseThrow(()-> new BusinessException("Data category tidak ada"));

        DailyRecords model = new DailyRecords();
        model.setDate(request.getDate());
        model.setType(type);
        model.setCategoryOut(category);
        model.setAmount(request.getAmount());
        model.setInfo(request.getInfo());
        DailyRecords saved = financialRecordsDao.save(model);
        return saved.getId();
    }

    public String updateOut (String id, FinancialRecordsRequest request) {
        log.info("FinancialRecordsService::update \nrequest json ={}", JsonUtils.objectToStringJson(request));
        DailyRecords model = financialRecordsDao.findById(id)
                .orElseThrow(() -> new BusinessException("Data id daily record tidak ada"));
        FinancialType type = financialTypeDao.findById(request.getType())
                .orElseThrow(() -> new BusinessException("Data type tidak ada"));
        CategoryOut category = categoryOutDao.findById(request.getCategory())
                .orElseThrow(() -> new BusinessException("Data category tidak ada"));

        model.setDate(request.getDate());
        model.setType(type);
        model.setCategoryOut(category);
        model.setAmount(request.getAmount());
        model.setInfo(request.getInfo());
        DailyRecords saved = financialRecordsDao.save(model);
        return saved.getId();
    }

    public String delete (String id) {
        log.info("FinancialRecordsService::delete {}", id);
        DailyRecords model = financialRecordsDao.findById(id)
                .orElseThrow(() -> new BusinessException("Data id daily record tidak ada"));
        model.setIsDeleted(true);
        DailyRecords saved = financialRecordsDao.save(model);
        return saved.getId();
    }
}
