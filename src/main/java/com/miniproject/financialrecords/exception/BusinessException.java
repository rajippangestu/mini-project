package com.miniproject.financialrecords.exception;
import com.miniproject.financialrecords.utils.GlobalMessage;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BusinessException extends RuntimeException {

    private String message;
    private List<String> causes;

    public BusinessException(String customMessage) {
        super(customMessage);
        this.message = customMessage;
    }

    public BusinessException(GlobalMessage globalMessage) {
        super(globalMessage.bahasa);
        this.message = globalMessage.bahasa;
    }

    public BusinessException(GlobalMessage globalMessage, List<String> causes) {
        super(globalMessage.bahasa);
        this.message = globalMessage.bahasa;
        this.causes = causes;
    }

    public static BusinessException dataNotFound() {
        List<String> causes = new ArrayList<>();
        causes.add("Data yang dicari memang tidak ada dalam database");
        causes.add("Page number melebihi jumlah data yang dicari");
        return new BusinessException(GlobalMessage.DATA_NOT_FOUND, causes);
    }

    public BusinessException(GlobalMessage globalMessage, String suffixAdditionalMessage) {
        super(globalMessage.bahasa + " " + suffixAdditionalMessage);
        this.message = globalMessage.bahasa + " " + suffixAdditionalMessage;
    }

    public BusinessException(String prefixAdditionalMessage, GlobalMessage globalMessage) {
        super(globalMessage.bahasa + " " + prefixAdditionalMessage);
        this.message = prefixAdditionalMessage + " " + globalMessage.bahasa;
    }

    public static BusinessException dataRelationNotFound(String entity) {
        return new BusinessException(entity, GlobalMessage.DATA_RELATION_NOT_FOUND);
    }

    public static BusinessException dataWithSamePropertyAlreadyExist(String property, String value) {
        return new BusinessException("Data yang sama dengan " + property + " " + value + " sudah ada dalam database");
    }

}
