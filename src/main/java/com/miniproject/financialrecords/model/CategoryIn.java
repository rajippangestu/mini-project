package com.miniproject.financialrecords.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "category_in")

public class CategoryIn {
    @Id
    @Column(name = "id")
    private String id;

    @Column (name = "name")
    private String name;
}
