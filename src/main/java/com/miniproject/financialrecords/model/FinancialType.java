package com.miniproject.financialrecords.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "type")
public class FinancialType {
    @Id
    @Column
    private String id;

    @Column(name = "name")
    private String name;
}
