package com.miniproject.financialrecords.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name = "daily_records")
@Where(clause = "is_deleted = false")

@NoArgsConstructor
@AllArgsConstructor
public class DailyRecords {
    @Id
    @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "date", nullable = false)
    private Date date;

    @ManyToOne
    @JoinColumn(name ="type_id", referencedColumnName = "id")
    private FinancialType type;

    @ManyToOne
    @JoinColumn(name ="cat_in", referencedColumnName = "id")
    private CategoryIn categoryIn;

    @ManyToOne
    @JoinColumn(name ="cat_out", referencedColumnName = "id")
    private CategoryOut categoryOut;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "information")
    private String info;

    @Column(name = "insert_at", updatable = false)
    @CreationTimestamp
    private Timestamp insertAt;

    @Column(name = "update_at")
    @UpdateTimestamp
    private Timestamp updateAt;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted;


}
