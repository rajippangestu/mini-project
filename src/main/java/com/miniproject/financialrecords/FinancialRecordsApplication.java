package com.miniproject.financialrecords;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinancialRecordsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinancialRecordsApplication.class, args);
	}

}
