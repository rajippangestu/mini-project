package com.miniproject.financialrecords.controller;

import com.miniproject.financialrecords.dao.DailyRecordsService;
import com.miniproject.financialrecords.utils.CsvFileGenerator;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@RestController
@RequestMapping
@AllArgsConstructor
@Controller
public class CSVController {
    @Autowired
    private DailyRecordsService dailyRecordsService;

    @Autowired
    private CsvFileGenerator csvFileGenerator;

    @GetMapping("/export-to-CSV")
    public void exportIntoCSV(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.addHeader("Content-Disposition", "attachment; filename=\"dailyReport.csv\"");
        csvFileGenerator.writeDailyToCSV(dailyRecordsService.getDailyRecordsOfList(), response.getWriter());
    }
}
