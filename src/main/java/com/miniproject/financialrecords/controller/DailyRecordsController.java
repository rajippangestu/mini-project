package com.miniproject.financialrecords.controller;

import com.miniproject.financialrecords.dto.BaseResponse;
import com.miniproject.financialrecords.dto.FinancialRecordsRequest;
import com.miniproject.financialrecords.dto.FinancialRecordsResponse;
import com.miniproject.financialrecords.dto.TypeRecordResponse;
import com.miniproject.financialrecords.service.FinancialRecordsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/financial-records")
@AllArgsConstructor
public class DailyRecordsController extends BaseController {
    private FinancialRecordsService financialRecordsService;

    @GetMapping("/page")
    public BaseResponse<Page<FinancialRecordsResponse>> findAllPage(@RequestParam(defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
                                                             @RequestParam(defaultValue = "#{T(java.time.LocalDate).now()}") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
                                                             @RequestParam(defaultValue = "0") int page,
                                                             @RequestParam(defaultValue = "10") int size) {
        return buildSuccessResponse(financialRecordsService.findAllPage( dateFrom, dateTo, PageRequest.of(page, size)));
    }

    @GetMapping("/page/in")
    public BaseResponse<Page<TypeRecordResponse>> findAllPageIn(@RequestParam(defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
                                                                    @RequestParam(defaultValue = "#{T(java.time.LocalDate).now()}") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
                                                                    @RequestParam(defaultValue = "0") int page,
                                                                    @RequestParam(defaultValue = "10") int size) {
        return buildSuccessResponse(financialRecordsService.findAllPageIn( dateFrom, dateTo, PageRequest.of(page, size)));
    }

    @GetMapping("/page/out")
    public BaseResponse<Page<TypeRecordResponse>> findAllPageOut(@RequestParam(defaultValue = "2022-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
                                                                 @RequestParam(defaultValue = "#{T(java.time.LocalDate).now()}") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
                                                                 @RequestParam(defaultValue = "0") int page,
                                                                 @RequestParam(defaultValue = "10") int size) {
        return buildSuccessResponse(financialRecordsService.findAllPageOut( dateFrom, dateTo, PageRequest.of(page, size)));
    }

    @GetMapping("/{id}")
    public BaseResponse<Optional<Map<String, Object>>> findById (@PathVariable("id") String id){
        return buildSuccessResponse(financialRecordsService.findRecordsById(id));
    }


//    in
    @PostMapping("/in")
    public BaseResponse<String> createIn (@RequestBody FinancialRecordsRequest request) {
        return buildSuccessResponse(financialRecordsService.createIn(request));
    }
    @PutMapping("/in/{id}")
    public BaseResponse<String> updateIn (@PathVariable("id") String id,
                                          @RequestBody FinancialRecordsRequest request){
        return  buildSuccessResponse(financialRecordsService.updateIn(id,request));
    }

//  out
    @PostMapping("/out")
    public BaseResponse<String> createOut (@RequestBody FinancialRecordsRequest request) {
        return buildSuccessResponse(financialRecordsService.createOut(request));
    }
    @PutMapping("/out/{id}")
    public BaseResponse<String> updateOut (@PathVariable("id") String id,
                                           @RequestBody FinancialRecordsRequest request) {
        return buildSuccessResponse(financialRecordsService.updateOut(id, request));
    }

//  delete
    @DeleteMapping("{id}")
    public BaseResponse<String> delete (@PathVariable("id") String id){
        return buildSuccessResponse(financialRecordsService.delete(id));
    }

}
