package com.miniproject.financialrecords.controller;

import com.miniproject.financialrecords.dto.BaseResponse;
import com.miniproject.financialrecords.dto.MetaResponse;
import org.springframework.http.HttpStatus;

public class BaseController {

    public <T> BaseResponse<T> buildSuccessResponse(T data) {
        MetaResponse meta = MetaResponse.builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK.name())
                .message("SUKSES")
                .build();
        return new BaseResponse<>(meta, data);
    }
}
