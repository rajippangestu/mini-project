package com.miniproject.financialrecords.dao;

import com.miniproject.financialrecords.model.DailyRecords;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

public interface FinancialRecordsDao extends JpaRepository <DailyRecords, String> {
    @Query(value = "select id,\n" +
            "\"date\",\n" +
            "type_id,\n" +
            "cat_in,\n" +
            "cat_out,\n" +
            "amount,\n" +
            "information \n" +
            "from daily_records dr\n" +
            "where is_deleted = false \n" , nativeQuery = true)
    Page<Map<String,Object>> findAll(LocalDate dateFrom, LocalDate dateTo, Pageable pageable);

    @Query(value = "select id,\n" +
            "\"date\",\n" +
            "cat_in,\n" +
            "amount,\n" +
            "information \n" +
            "from daily_records dr\n" +
            "where is_deleted = false \n" +
            "and dr.type_id = '1'", nativeQuery = true)
    Page<Map<String,Object>> findAllIn(LocalDate dateFrom, LocalDate dateTo, Pageable pageable);

    @Query(value = "select id,\n" +
            "\"date\",\n" +
            "cat_out,\n" +
            "amount,\n" +
            "information \n" +
            "from daily_records dr\n" +
            "where is_deleted = false \n" +
            "and dr.type_id = '2'" , nativeQuery = true)
    Page<Map<String,Object>> findAllOut(LocalDate dateFrom, LocalDate dateTo, Pageable pageable);

    @Query(value ="select id,\n" +
            "\"date\",\n" +
            "type_id,\n" +
            "cat_in,\n" +
            "cat_out,\n" +
            "amount,\n" +
            "information \n" +
            "from daily_records dr\n" +
            "where is_deleted = false \n" +
            "and dr.id = :id", nativeQuery = true)
    Optional<Map<String, Object>> findRecordDailyById(String id);

}


