package com.miniproject.financialrecords.dao;

import com.miniproject.financialrecords.model.FinancialType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FinancialTypeDao extends JpaRepository<FinancialType,String> {
}
