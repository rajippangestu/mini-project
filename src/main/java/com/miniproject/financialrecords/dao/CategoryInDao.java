package com.miniproject.financialrecords.dao;

import com.miniproject.financialrecords.model.CategoryIn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryInDao extends JpaRepository<CategoryIn, String> {
}
