package com.miniproject.financialrecords.dao;

import com.miniproject.financialrecords.model.DailyRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DailyRecordsImpl implements DailyRecordsService{
    @Autowired
    DailyRecordsCSV dailyRecordsCSV;
    @Override
    public void addDailyRecords(DailyRecords dailyRecords) {
        dailyRecordsCSV.save(dailyRecords);
    }

    @Override
    public List<DailyRecords> getDailyRecordsOfList() {
        return dailyRecordsCSV.findAll();
    }

}
