package com.miniproject.financialrecords.dao;

import com.miniproject.financialrecords.model.DailyRecords;

import java.util.List;

public interface DailyRecordsService {
    void addDailyRecords (DailyRecords dailyRecords);

    List<DailyRecords> getDailyRecordsOfList();

}
