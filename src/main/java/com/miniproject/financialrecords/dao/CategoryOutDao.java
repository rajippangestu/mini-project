package com.miniproject.financialrecords.dao;

import com.miniproject.financialrecords.model.CategoryOut;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryOutDao extends JpaRepository<CategoryOut, String> {
}