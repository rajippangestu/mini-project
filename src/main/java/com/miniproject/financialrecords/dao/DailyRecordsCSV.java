package com.miniproject.financialrecords.dao;

import com.miniproject.financialrecords.model.DailyRecords;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DailyRecordsCSV extends JpaRepository<DailyRecords, String> {
}
