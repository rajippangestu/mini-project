package com.miniproject.financialrecords.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MapConverterUtils {

    public static boolean setLikeMap(Object like) {
        return Objects.nonNull(like);
    }

    public static int setIntMap(Object duration) {
        return Optional.ofNullable(duration).map(Object::toString).map(Integer::valueOf).orElse(0);
    }

    public static Long setLongMap(Object duration) {
        return Optional.ofNullable(duration).map(Object::toString).map(Long::valueOf).orElse(0L);
    }

    public static String setStringMap(Object musicUrl) {
        return Optional.ofNullable(musicUrl).map(Object::toString).orElse(null);
    }

    public static LocalDate setDateMap(Object date) {
        return Optional.ofNullable(date).map(Object::toString).map(LocalDate::parse).orElse(LocalDate.now());
    }

    public static Float setFloatMap(Object duration) {
        return Optional.ofNullable(duration).map(Object::toString).map(Float::valueOf).orElse(0f);
    }

    public static BigDecimal setBigDecimalMap(Object bigDecimal) {
        return Optional.ofNullable(bigDecimal).map(Object::toString).map(BigDecimal::new).orElse(BigDecimal.ZERO);
    }

    public static boolean setBooleanMap(Object oBoolean) {
        return Boolean.TRUE.equals(Optional.ofNullable(oBoolean).map(Object::toString).map(Boolean::valueOf).orElse(false));
    }

    public static Date setLocalDate(Object object) {
        return Date.valueOf(Optional.ofNullable(object).map(Object::toString).map(date -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            return LocalDate.parse(date, formatter);
        }).orElse(null));
    }

    public static LocalDateTime setLocalDateTime(Object object) {
        return Optional.ofNullable(object).map(Object::toString).map(s -> LocalDateTime.parse(s.split("\\.")[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).orElse(null);
    }
}
