package com.miniproject.financialrecords.utils;

public enum GlobalMessage {

    SUCCESS("Sukses"),
    DATA_NOT_FOUND("Data tidak ditemukan"),
    DATA_RELATION_NOT_FOUND("data relasi tidak ditemukan"),
    DATA_ALREADY_EXIST("Data sudah ada"),
    DATA_WITH_SAME_PROPERTY_ALREADY_EXIST("yang sama sudah ada dalam database"),
    HTTP_PARAMETER_NOT_VALID("Parameter HTTP request tidak sesuai"),
    CONTACT_OUR_TEAM("Hubungi tim kami dan kirim kode ini ke tim kami : "),
    FILE_MUST_BE_INCLUDED("File tidak boleh kosong");

    public String bahasa;

    GlobalMessage(String bahasa) {
        this.bahasa = bahasa;
    }
}
