package com.miniproject.financialrecords.utils;

import com.miniproject.financialrecords.model.DailyRecords;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

@Component
public class CsvFileGenerator {
    public void writeDailyToCSV(List<DailyRecords> dailyRecords, Writer writer) {
        try {
            CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT);
            for (DailyRecords daily : dailyRecords) {
                printer.printRecords(daily.getId(),
                        daily.getDate(),
                        daily.getType(),
                        daily.getCategoryIn(),
                        daily.getCategoryOut(),
                        daily.getAmount(),
                        daily.getInfo());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
