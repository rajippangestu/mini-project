package com.miniproject.financialrecords.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ObjectConverter {

    public static boolean mapToBooleanPrimitive(Object bool) {
        return Objects.nonNull(bool);
    }

    public static int mapToInt(Object duration) {
        return Optional.ofNullable(duration).map(Object::toString).map(Integer::valueOf).orElse(0);
    }

    public static Long mapToLong(Object duration) {
        return Optional.ofNullable(duration).map(Object::toString).map(Long::valueOf).orElse(0L);
    }

    public static String mapToString(Object string) {
        return Optional.ofNullable(string).map(Object::toString).orElse(null);
    }

    public static LocalDate mapToLocalDate(Object date) {
        return Optional.ofNullable(date).map(Object::toString).map(LocalDate::parse).orElse(LocalDate.now());
    }

    public static Float mapToFloat(Object duration) {
        return Optional.ofNullable(duration).map(Object::toString).map(Float::valueOf).orElse(0f);
    }

    public static BigDecimal mapToBigDecimal(Object bigDecimal) {
        return Optional.ofNullable(bigDecimal).map(Object::toString).map(BigDecimal::new).orElse(BigDecimal.ZERO);
    }

    public static boolean mapToBoolean(Object oBoolean) {
        return Boolean.TRUE.equals(Optional.ofNullable(oBoolean).map(Object::toString).map(Boolean::valueOf).orElse(false));
    }

    public static LocalDateTime mapToLocalDateTime(Object object) {
        return Optional.ofNullable(object).map(Object::toString).map(s -> LocalDateTime.parse(s.split("\\.")[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).orElse(null);
    }
}
