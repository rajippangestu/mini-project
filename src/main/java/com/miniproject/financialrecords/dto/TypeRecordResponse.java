package com.miniproject.financialrecords.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TypeRecordResponse {
    private String id;
    private Date date;
    private String category;
    private BigDecimal amount;
    private String info;
}
