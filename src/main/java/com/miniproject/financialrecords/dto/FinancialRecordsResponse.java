package com.miniproject.financialrecords.dto;

import lombok.*;
import java.math.BigDecimal;
import java.sql.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FinancialRecordsResponse {
    private String id;
    private Date date;
    private String type;
    private String categoryIn;
    private String categoryOut;
    private BigDecimal amount;
    private String info;

}
