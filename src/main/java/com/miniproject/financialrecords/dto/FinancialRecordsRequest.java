package com.miniproject.financialrecords.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class FinancialRecordsRequest {
    private String id;
    private Date date;
    private String type;
    private String category;
    private BigDecimal amount;
    private String info;
}
